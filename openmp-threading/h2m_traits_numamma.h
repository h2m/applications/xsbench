#ifndef __H2M_TRAITS_NUMAMMA_H__
#define __H2M_TRAITS_NUMAMMA_H__

#include "h2m.h"

static h2m_alloc_trait_t* get_traits_nuclide(int* n_traits) {
    *n_traits = 8;
    h2m_alloc_trait_t traits[8] = {
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded, 
        h2m_atk_access_mode, h2m_atv_access_mode_readonly, 
        h2m_atk_access_freq, { .dbl = 2904.314888122524}, // Accesses per sec
        h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch, 
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear, 
        h2m_atk_access_stride, 1, 
        h2m_atk_access_prio, { .dbl = 0.6164222116444967},
        h2m_atk_sensitivity, h2m_atv_lat_sensitive,
    };
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits, tmp_size);
    return ret;
}

static h2m_alloc_trait_t* get_traits_unionized(int* n_traits) {
    *n_traits = 8;
    h2m_alloc_trait_t traits[8] = {
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded, 
        h2m_atk_access_mode, h2m_atv_access_mode_readonly, 
        h2m_atk_access_freq, { .dbl = 322.36313621889354}, // Accesses per sec
        h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch, 
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear, 
        h2m_atk_access_stride, 1, 
        h2m_atk_access_prio, { .dbl = 0.005952166294975091}, 
        h2m_atk_sensitivity, h2m_atv_bw_sensitive, 
    };
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits, tmp_size);
    return ret;
}

static h2m_alloc_trait_t* get_traits_idx(int* n_traits) {
    *n_traits = 8;
    h2m_alloc_trait_t traits[8] = {
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded, 
        h2m_atk_access_mode, h2m_atv_access_mode_readwrite, 
        h2m_atk_access_freq, { .dbl = 1815.5740716363182}, // Accesses per sec
        h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch, 
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear, 
        h2m_atk_access_stride, 1, 
        h2m_atk_access_prio, { .dbl = 0.004937161523687504}, 
        h2m_atk_sensitivity, h2m_atv_bw_sensitive, 
    };

    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits, tmp_size);
    return ret;
}

#endif