#ifndef __H2M_TRAITS_EXPERIMENTAL_H__
#define __H2M_TRAITS_EXPERIMENTAL_H__

#include "h2m.h"

static h2m_alloc_trait_value_t get_mem_space(const char* env_name) {
    h2m_alloc_trait_value_t ret = h2m_atv_mem_space_large_cap;
    char* tmp = getenv(env_name);
    if (tmp) {
        if(strcmp(tmp, "HBW") == 0) {
            ret = h2m_atv_mem_space_hbw;
        }
    }
    printf("Returned mem space %d\n", ret);
    return ret;
}

static h2m_alloc_trait_t* get_traits_nuclide(int* n_traits) {
    h2m_alloc_trait_value_t ms = get_mem_space("MEMSPACE_NUCLIDE");
    *n_traits = 1;
    h2m_alloc_trait_t traits_nuclide[1] = {h2m_atk_req_mem_space, {.atv = ms}};
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits_nuclide, tmp_size);
    return ret;
}

static h2m_alloc_trait_t* get_traits_unionized(int* n_traits) {
    h2m_alloc_trait_value_t ms = get_mem_space("MEMSPACE_UNI");
    *n_traits = 1;
    h2m_alloc_trait_t traits_unionized[1] = {h2m_atk_req_mem_space, {.atv = ms}};
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits_unionized, tmp_size);
    return ret;
}

static h2m_alloc_trait_t* get_traits_idx(int* n_traits) {
    h2m_alloc_trait_value_t ms = get_mem_space("MEMSPACE_IDX");
    *n_traits = 1;
    h2m_alloc_trait_t traits_idx[1] = {h2m_atk_req_mem_space, {.atv = ms}};
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits_idx, tmp_size);
    return ret;
}

#endif