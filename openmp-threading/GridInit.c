#include "XSbench_header.h"

#if USE_H2M_ALLOC_ABSTRACTION
#if LOAD_TRAIT_VERSION == 0
#include "h2m_traits_manual.h"
#elif LOAD_TRAIT_VERSION == 1
#include "h2m_traits_numamma.h"
#else
// #include "h2m_traits_asan.h"
#endif // LOAD_TRAIT_VERSION
#endif // USE_H2M_ALLOC_ABSTRACTION

long len_per_split = 0;

SimulationData grid_alloc( Inputs in, int mype )
{
	// Structure to hold all allocated simuluation data arrays
	SimulationData SD;

	// Keep track of how much data we're allocating
	size_t nbytes = 0;

#if USE_H2M_ALLOC_ABSTRACTION
    int err = 0;
#   if LOAD_TRAIT_VERSION >= 0 && LOAD_TRAIT_VERSION < 3
    int ndecl = 0;
    int n_traits_nuclide, n_traits_unionized, n_traits_idx;
    h2m_declaration_t alloc_decl[3+N_SPLITS];    

    h2m_alloc_trait_t* traits_nuclide_grid  = get_traits_nuclide(&n_traits_nuclide);
    h2m_alloc_trait_t* traits_unionized     = get_traits_unionized(&n_traits_unionized);
    h2m_alloc_trait_t* traits_idx           = get_traits_idx(&n_traits_idx);

    err += h2m_verify_traits(n_traits_nuclide, traits_nuclide_grid);
    err += h2m_verify_traits(n_traits_unionized, traits_unionized);
    err += h2m_verify_traits(n_traits_idx, traits_idx);
    if(err != H2M_SUCCESS) assert(0);
#   endif // LOAD_TRAIT_VERSION
#endif // USE_H2M_ALLOC_ABSTRACTION

	////////////////////////////////////////////////////////////////////
	// Initialize Nuclide Grids
	////////////////////////////////////////////////////////////////////
	
	if(mype == 0) printf("Allocating nuclide grids...\n");

	// First, we need to initialize our nuclide grid. This comes in the form
	// of a flattened 2D array that hold all the information we need to define
	// the cross sections for all isotopes in the simulation. 
	// The grid is composed of "NuclideGridPoint" structures, which hold the
	// energy level of the grid point and all associated XS data at that level.
	// An array of structures (AOS) is used instead of
	// a structure of arrays, as the grid points themselves are accessed in 
	// a random order, but all cross section interaction channels and the
	// energy level are read whenever the gridpoint is accessed, meaning the
	// AOS is more cache efficient.

	////////////////////////////////////////////////////////////////////
	// Initialize Acceleration Structure
	////////////////////////////////////////////////////////////////////
	if( in.grid_type == UNIONIZED )
	{
		if(mype == 0) printf("Allocating unionized grid...\n");

		// Allocate space to hold the union of all nuclide energy data
		SD.length_unionized_energy_array = in.n_isotopes * in.n_gridpoints;
#if USE_H2M_ALLOC_ABSTRACTION
#   if LOAD_TRAIT_VERSION == -1
        SD.unionized_energy_array = (double *) h2m_alloc(SD.length_unionized_energy_array * sizeof(double), &err);
#   elif LOAD_TRAIT_VERSION == 3
        SD.unionized_energy_array = (double *) h2m_alloc_w_traits_file(SD.length_unionized_energy_array * sizeof(double), 6, &err);
#   else // LOAD_TRAIT_VERSION
        alloc_decl[ndecl] = h2m_declare_alloc_w_traits((void**)(&SD.unionized_energy_array), SD.length_unionized_energy_array * sizeof(double), NULL, NULL, &err, n_traits_unionized, traits_unionized);
        ndecl++;
#   endif // LOAD_TRAIT_VERSION
#else
		SD.unionized_energy_array = (double *) malloc( SD.length_unionized_energy_array * sizeof(double));
#endif
		// assert(SD.unionized_energy_array != NULL );
		nbytes += SD.length_unionized_energy_array * sizeof(double);

		// Allocate space to hold the acceleration grid indices
		SD.length_index_grid = SD.length_unionized_energy_array * in.n_isotopes;
		if(mype == 0) printf("length_index_grid=%ld; n_isotopes=%ld, length_unionized_energy_array=%d; N_SPLITS=%d\n", SD.length_index_grid, in.n_isotopes, SD.length_unionized_energy_array, N_SPLITS);

#if !SPLIT_ALLOCATIONS
		SD.index_grid = (int *) malloc( SD.length_index_grid * sizeof(int));
		assert(SD.index_grid != NULL);
#else // SPLIT_ALLOCATIONS
		len_per_split = SD.length_index_grid / N_SPLITS;
		if(SD.length_index_grid % N_SPLITS != 0) {
			len_per_split++;
		}
		if(mype == 0) printf("allocating index_grid with len_per_split=%ld\n", len_per_split);
#if USE_H2M_ALLOC_ABSTRACTION
#   if LOAD_TRAIT_VERSION == -1
        SD.index_grid = (int **) h2m_alloc(N_SPLITS * sizeof(int*), &err);
#   elif LOAD_TRAIT_VERSION == 3
        SD.index_grid = (int **) h2m_alloc_w_traits_file(N_SPLITS * sizeof(int*), 4, &err);
#   else // LOAD_TRAIT_VERSION
        SD.index_grid = (int **) h2m_alloc_w_traits(N_SPLITS * sizeof(int*), &err, n_traits_idx, traits_idx);
#   endif // LOAD_TRAIT_VERSION
#else
		SD.index_grid = (int **) malloc(N_SPLITS * sizeof(int*));
#endif
		assert(SD.index_grid != NULL);
		for(int i = 0; i < N_SPLITS; i++) {
#if USE_H2M_ALLOC_ABSTRACTION
#   if LOAD_TRAIT_VERSION == -1
            SD.index_grid[i] = (int *) h2m_alloc(len_per_split * sizeof(int), &err);
#   elif LOAD_TRAIT_VERSION == 3
            SD.index_grid[i] = (int *) h2m_alloc_w_traits_file(len_per_split * sizeof(int), 4, &err);
#   else
            alloc_decl[ndecl] = h2m_declare_alloc_w_traits((void**)(&(SD.index_grid[i])), len_per_split * sizeof(int), NULL, NULL, &err, n_traits_idx, traits_idx);
            ndecl++;
#   endif // LOAD_TRAIT_VERSION
#else
			SD.index_grid[i] = (int *) malloc(len_per_split * sizeof(int));
#endif
		}
#endif
		nbytes += SD.length_index_grid * sizeof(int);
	}

    // Initialize Nuclide Grid
	SD.length_nuclide_grid = in.n_isotopes * in.n_gridpoints;
#if USE_H2M_ALLOC_ABSTRACTION
#   if LOAD_TRAIT_VERSION == -1
    SD.nuclide_grid = (NuclideGridPoint *) h2m_alloc(SD.length_nuclide_grid * sizeof(NuclideGridPoint), &err);
#   elif LOAD_TRAIT_VERSION == 3
    SD.nuclide_grid = (NuclideGridPoint *) h2m_alloc_w_traits_file(SD.length_nuclide_grid * sizeof(NuclideGridPoint), 5, &err);
#   else // LOAD_TRAIT_VERSION
    alloc_decl[ndecl] = h2m_declare_alloc_w_traits((void**)(&SD.nuclide_grid), SD.length_nuclide_grid * sizeof(NuclideGridPoint), NULL, NULL, &err, n_traits_nuclide, traits_nuclide_grid);
    ndecl++;
#   endif // LOAD_TRAIT_VERSION
#else
	SD.nuclide_grid = (NuclideGridPoint *) malloc( SD.length_nuclide_grid * sizeof(NuclideGridPoint));
#endif
	nbytes += SD.length_nuclide_grid * sizeof(NuclideGridPoint);

#if USE_H2M_ALLOC_ABSTRACTION && LOAD_TRAIT_VERSION >= 0 && LOAD_TRAIT_VERSION < 3
    err = h2m_commit_allocs(alloc_decl, ndecl);
    if(err != H2M_SUCCESS) assert(0);
#endif
	
	if(mype == 0) printf("Allocation complete. Allocated %.0lf MB of data.\n", nbytes/1024.0/1024.0 );
	return SD;
}

SimulationData grid_init_do_not_profile( SimulationData SD, Inputs in, int mype )
{
	// Set the initial seed value
	uint64_t seed = 42;	

	////////////////////////////////////////////////////////////////////
	// Initialize Nuclide Grids
	////////////////////////////////////////////////////////////////////
	
	if(mype == 0) printf("Intializing nuclide grids...\n");

	// First, we need to initialize our nuclide grid. This comes in the form
	// of a flattened 2D array that hold all the information we need to define
	// the cross sections for all isotopes in the simulation. 
	// The grid is composed of "NuclideGridPoint" structures, which hold the
	// energy level of the grid point and all associated XS data at that level.
	// An array of structures (AOS) is used instead of
	// a structure of arrays, as the grid points themselves are accessed in 
	// a random order, but all cross section interaction channels and the
	// energy level are read whenever the gridpoint is accessed, meaning the
	// AOS is more cache efficient.
	
	// Initialize Nuclide Grid
	for( int i = 0; i < SD.length_nuclide_grid; i++ )
	{
		SD.nuclide_grid[i].energy        = LCG_random_double(&seed);
		SD.nuclide_grid[i].total_xs      = LCG_random_double(&seed);
		SD.nuclide_grid[i].elastic_xs    = LCG_random_double(&seed);
		SD.nuclide_grid[i].absorbtion_xs = LCG_random_double(&seed);
		SD.nuclide_grid[i].fission_xs    = LCG_random_double(&seed);
		SD.nuclide_grid[i].nu_fission_xs = LCG_random_double(&seed);
	}

	// Sort so that each nuclide has data stored in ascending energy order.
	for( int i = 0; i < in.n_isotopes; i++ )
		qsort( &SD.nuclide_grid[i*in.n_gridpoints], in.n_gridpoints, sizeof(NuclideGridPoint), NGP_compare);
	
	// error debug check
	/*
	for( int i = 0; i < in.n_isotopes; i++ )
	{
		printf("NUCLIDE %d ==============================\n", i);
		for( int j = 0; j < in.n_gridpoints; j++ )
			printf("E%d = %lf\n", j, SD.nuclide_grid[i * in.n_gridpoints + j].energy);
	}
	*/
	

	////////////////////////////////////////////////////////////////////
	// Initialize Acceleration Structure
	////////////////////////////////////////////////////////////////////
	
	if( in.grid_type == NUCLIDE )
	{
		SD.length_unionized_energy_array = 0;
		SD.length_index_grid = 0;
	}
	
	if( in.grid_type == UNIONIZED )
	{
		if(mype == 0) printf("Intializing unionized grid...\n");

		// Copy energy data over from the nuclide energy grid
		for( int i = 0; i < SD.length_unionized_energy_array; i++ )
			SD.unionized_energy_array[i] = SD.nuclide_grid[i].energy;

		// Sort unionized energy array
		qsort( SD.unionized_energy_array, SD.length_unionized_energy_array, sizeof(double), double_compare);

		// Generates the double indexing grid
		int * idx_low = (int *) calloc( in.n_isotopes, sizeof(int));
		assert(idx_low != NULL );
		double * energy_high = (double *) malloc( in.n_isotopes * sizeof(double));
		assert(energy_high != NULL );

		for( int i = 0; i < in.n_isotopes; i++ )
			energy_high[i] = SD.nuclide_grid[i * in.n_gridpoints + 1].energy;

		long idx1, idx2;
		for( long e = 0; e < SD.length_unionized_energy_array; e++ )
		{
			double unionized_energy = SD.unionized_energy_array[e];
			for( long i = 0; i < in.n_isotopes; i++ )
			{
				map_index(e * in.n_isotopes + i, &idx1, &idx2);
				if( unionized_energy < energy_high[i]  ) {
#if !SPLIT_ALLOCATIONS
					SD.index_grid[e * in.n_isotopes + i] = idx_low[i];
#else
					SD.index_grid[idx1][idx2] = idx_low[i];
#endif // SPLIT_ALLOCATIONS
				} else if( idx_low[i] == in.n_gridpoints - 2 ) {
#if !SPLIT_ALLOCATIONS
					SD.index_grid[e * in.n_isotopes + i] = idx_low[i];
#else
					SD.index_grid[idx1][idx2] = idx_low[i];
#endif // SPLIT_ALLOCATIONS
				} else {
					idx_low[i]++;
#if !SPLIT_ALLOCATIONS
					SD.index_grid[e * in.n_isotopes + i] = idx_low[i];
#else
					SD.index_grid[idx1][idx2] = idx_low[i];
#endif // SPLIT_ALLOCATIONS
					energy_high[i] = SD.nuclide_grid[i * in.n_gridpoints + idx_low[i] + 1].energy;
				}
			}
		}

		free(idx_low);
		free(energy_high);
	}

	////////////////////////////////////////////////////////////////////
	// Initialize Materials and Concentrations
	////////////////////////////////////////////////////////////////////
	if(mype == 0) printf("Intializing material data...\n");
	
	// Set the number of nuclides in each material
	SD.num_nucs  = load_num_nucs(in.n_isotopes);
	SD.length_num_nucs = 12; // There are always 12 materials in XSBench

	// Intialize the flattened 2D grid of material data. The grid holds
	// a list of nuclide indices for each of the 12 material types. The
	// grid is allocated as a full square grid, even though not all
	// materials have the same number of nuclides.
	SD.mats = load_mats(SD.num_nucs, in.n_isotopes, &SD.max_num_nucs);
	SD.length_mats = SD.length_num_nucs * SD.max_num_nucs;

	// Intialize the flattened 2D grid of nuclide concentration data. The grid holds
	// a list of nuclide concentrations for each of the 12 material types. The
	// grid is allocated as a full square grid, even though not all
	// materials have the same number of nuclides.
	SD.concs = load_concs(SD.num_nucs, SD.max_num_nucs);
	SD.length_concs = SD.length_mats;

	if(mype == 0) printf("Intialization complete.\n");

    return SD;
}

void map_index(long orig_index, long* idx1, long* idx2) {
	*idx1 = orig_index / len_per_split;
	*idx2 = orig_index % len_per_split;
}
