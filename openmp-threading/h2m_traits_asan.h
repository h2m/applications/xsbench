#ifndef __H2M_TRAITS_ASAN_H__
#define __H2M_TRAITS_ASAN_H__

#include "h2m.h"

static h2m_alloc_trait_t* get_traits_nuclide(int* n_traits) {
    *n_traits = 10;
    h2m_alloc_trait_t traits_nuclide[10] = {
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_freq, { .dbl = 2373266.917229613 }, // Accesses per sec
        h2m_atk_access_prio, { .dbl = 1.0 }, // Can be used for user-defined strategies
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear,
        h2m_atk_access_stride, 1,
        h2m_atk_custom1, { .l = 9969413 }, // Number of reads from buffer in phase
        h2m_atk_custom2, { .l = 0 }, // Number of writes to buffer in phase 
        h2m_atk_custom3, { .l = 12407725 }, // Number of all mem accesses in phase
        h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch,
    };
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits_nuclide, tmp_size);
    return ret;
}

static h2m_alloc_trait_t* get_traits_unionized(int* n_traits) {
    *n_traits = 9;
    h2m_alloc_trait_t traits_unionized[9] = {
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_freq, { .dbl = 52246.96190401757 }, // Accesses per sec
        h2m_atk_access_prio, { .dbl = 1.0 }, // Can be used for user-defined strategies
        h2m_atk_access_pattern, h2m_atv_access_pattern_random,
        h2m_atk_custom1, { .l = 219471 }, // Number of reads from buffer in phase
        h2m_atk_custom2, { .l = 0 }, // Number of writes to buffer in phase 
        h2m_atk_custom3, { .l = 12407725 }, // Number of all mem accesses in phase
        h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch,
    };
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits_unionized, tmp_size);
    return ret;
}

static h2m_alloc_trait_t* get_traits_idx(int* n_traits) {
    *n_traits = 10;
    h2m_alloc_trait_t traits_idx[10] = {
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_freq, { .dbl = 264799.88660992496 }, // Accesses per sec
        h2m_atk_access_prio, { .dbl = 1.0 }, // Can be used for user-defined strategies
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear,
        h2m_atk_access_stride, 1,
        h2m_atk_custom1, { .l = 1112293 }, // Number of reads from buffer in phase
        h2m_atk_custom2, { .l = 0 }, // Number of writes to buffer in phase 
        h2m_atk_custom3, { .l = 12407725 }, // Number of all mem accesses in phase
        h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch,
    };
    
    size_t tmp_size = sizeof(h2m_alloc_trait_t)* (*n_traits);
    h2m_alloc_trait_t* ret = (h2m_alloc_trait_t*) malloc(tmp_size);
    memcpy(ret, traits_idx, tmp_size);
    return ret;
}

#endif