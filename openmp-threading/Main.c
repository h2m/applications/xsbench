#include "XSbench_header.h"

#ifdef MPI
#include<mpi.h>
#endif

#if USE_LLVM_APPROACH
#include "h2m_mem_profiling.h"
#endif // USE_LLVM_APPROACH

int main( int argc, char* argv[] )
{
	// =====================================================================
	// Initialization & Command Line Read-In
	// =====================================================================
	int version = 20;
	int mype = 0;
	double time_data_init, time_simulation;
	int nprocs = 1;
	unsigned long long verification;

	#ifdef MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &mype);
	#endif

    #if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_init();
    #endif

	// Process CLI Fields -- store in "Inputs" structure
	Inputs in = read_CLI( argc, argv );

	// Set number of OpenMP Threads
	#ifdef OPENMP
	omp_set_num_threads(in.nthreads); 
	#endif

	// Print-out of Input Summary
	if( mype == 0 )
		print_inputs( in, nprocs, version );

	// =====================================================================
	// Prepare Nuclide Energy Grids, Unionized Energy Grid, & Material Data
	// This is not reflective of a real Monte Carlo simulation workload,
	// therefore, do not profile this region!
	// =====================================================================
	
    #if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_phase_transition("Allocation/Initialization");
    #endif // RECORD_TRANSITIONS
    #if USE_LLVM_APPROACH
    h2m_mp_new_phase("Allocation/Initialization");
    h2m_mp_set_access_profiling_enabled(0);
    #endif // USE_LLVM_APPROACH

	SimulationData SD;

    time_data_init = get_time();
	// If read from file mode is selected, skip initialization and load
	// all simulation data structures from file instead
	if( in.binary_mode == READ ) {
		SD = binary_read(in);
	} else {
		SD = grid_alloc( in, mype );
    	SD = grid_init_do_not_profile( SD, in, mype );
    }

	// If writing from file mode is selected, write all simulation data
	// structures to file
	if( in.binary_mode == WRITE && mype == 0 )
		binary_write(in, SD);

    time_data_init = get_time() - time_data_init;

	// =====================================================================
	// Cross Section (XS) Parallel Lookup Simulation
	// This is the section that should be profiled, as it reflects a 
	// realistic continuous energy Monte Carlo macroscopic cross section
	// lookup kernel.
	// =====================================================================

    #if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_phase_transition("Simulation");
    #endif // RECORD_TRANSITIONS
    #if USE_LLVM_APPROACH
    h2m_mp_new_phase("Simulation");
    h2m_mp_set_access_profiling_enabled(1);
    #endif // USE_LLVM_APPROACH

	if( mype == 0 )
	{
		printf("\n");
		border_print();
		center_print("SIMULATION", 79);
		border_print();
	}

	// Start Simulation Timer
	time_simulation = get_time();

	// Run simulation
	if( in.simulation_method == EVENT_BASED )
	{
		if( in.kernel_id == 0 )
			verification = run_event_based_simulation(in, SD, mype);
		else if( in.kernel_id == 1 )
			verification = run_event_based_simulation_optimization_1(in, SD, mype);
		else
		{
			printf("Error: No kernel ID %d found!\n", in.kernel_id);
			exit(1);
		}
	}
	else
		verification = run_history_based_simulation(in, SD, mype);

	if( mype == 0)	
	{	
		printf("\n" );
		printf("Simulation complete.\n" );
	}

	// End Simulation Timer
	time_simulation = get_time() - time_simulation;

	// =====================================================================
	// Output Results & Finalize
	// =====================================================================

    #if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_phase_transition("Finalization");
    #endif // RECORD_TRANSITIONS
    #if USE_LLVM_APPROACH
    h2m_mp_new_phase("Finalization");
    #endif // USE_LLVM_APPROACH

	// Final Hash Step
	verification = verification % 999983;

	// Print / Save Results and Exit
	int is_invalid_result = print_results( in, mype, time_data_init, time_simulation, nprocs, verification );

    #if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_finalize();
    #endif

	#ifdef MPI
	MPI_Finalize();
	#endif

	// return is_invalid_result;
	return 0;
}
